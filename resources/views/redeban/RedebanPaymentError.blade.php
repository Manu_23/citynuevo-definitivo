<!DOCTYPE html>
<html lang="{{setting('language','es')}}" dir="ltr">
<head>
    <meta charset="UTF-8">
    <title>{{setting('app_name')}} | {{setting('app_short_description')}}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="icon" type="image/png" href="{{$app_logo}}"/>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

</head>

<body style="height: 100%; background-color: #f9f9f9;" class="vh-100">

<div class="container vh-100">
    <div class="text-center">
        <input class="img-fluid p-5" type="image" src="https://icon-library.com/images/failed-icon/failed-icon-7.jpg" alt="error">
        <br>
        <h1 class="text-danger">Error en el pago</h1>
    </div>
</div>

</body>
