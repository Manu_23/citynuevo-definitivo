<?php

use Illuminate\Database\Seeder;
use App\Models\Subcategory;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = array(
            [
                'id' => 1,
                'category_id' => 1,
                'name' => 'Subcategoria 1'
            ],
            [
                'id' => 2,
                'category_id' => 1,
                'name' => 'Subcategoria 2'
            ],
            [
                'id' => 3,
                'category_id' => 1,
                'name' => 'Subcategoria 3'
            ],
            [
                'id' => 4,
                'category_id' => 2,
                'name' => 'Subcategoria 4'
            ],
            [
                'id' => 5,
                'category_id' => 2,
                'name' => 'Subcategoria 5'
            ],
            [
                'id' => 6,
                'category_id' => 2,
                'name' => 'Subcategoria 6'
            ],
            [
                'id' => 7,
                'category_id' => 3,
                'name' => 'Subcategoria 7'
            ],
            [
                'id' => 8,
                'category_id' => 3,
                'name' => 'Subcategoria 8'
            ],
            [
                'id' => 9,
                'category_id' => 3,
                'name' => 'Subcategoria 9'
            ],
        );
        foreach ($items as $key => $item) {
            Subcategory::updateOrCreate(['id' => $item['id']], $item);
        }
    }
}
