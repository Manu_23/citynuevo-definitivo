<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Vehicles;
use Faker\Generator as Faker;

$factory->define(Vehicles::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'image' => $faker->word,
        'role' => $faker->word,
        'desc' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
