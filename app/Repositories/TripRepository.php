<?php

namespace App\Repositories;

use App\Models\Trip;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TripRepository
 * @package App\Repositories
 * @version July 8, 2020, 10:58 pm UTC
 *
 * @method Trip findWithoutFail($id, $columns = ['*'])
 * @method Trip find($id, $columns = ['*'])
 * @method Trip first($columns = ['*'])
 */
class TripRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'address_pickup', 'address_leave'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Trip::class;
    }
}

