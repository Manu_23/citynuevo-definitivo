<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcategory extends Model
{
    protected $table = 'subcategories';
    use SoftDeletes;

    public function category(){
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }
}
