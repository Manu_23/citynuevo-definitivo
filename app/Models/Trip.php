<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $fillable = [
        'card', 'address_pickup', 'latitude_pickup', 'longitude_pickup', 'address_leave',
        'latitude_leave', 'longitude_leave', 'amount', 'user_id', 'driver_id', 'status'
    ];




}
