<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\IPNStatus;
use App\Item;
use App\Models\Payment;
use App\Models\User;
use App\Notifications\NewOrder;
use App\Repositories\CartRepository;
use App\Repositories\NotificationRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\ProductOrderRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Prettus\Validator\Exceptions\ValidatorException;
use Srmklive\PayPal\Services\ExpressCheckout;

class RedebanController extends ParentOrderController
{



    protected $provider;

    public function __init()
    {
        $this->provider = new ExpressCheckout();

    }




    public function index() {
        return view('welcome');
    }


    public function paymentTrip(Request $request){
        $trip = $request->get('trip');
        $total = $request->get('total');
        $user = $this->userRepository->findByField('api_token', $request->get('api_token'))->first();
        if (!empty($user)) {
            $this->order->user = $user;

            try {
                return view('redeban.trips.RedebanPaymentTrip', compact('trip', 'user', 'total'));
            } catch (\Exception $e) {
                session()->put(['code' => 'danger', 'message' => "Error processing payment for your order"]);
            }
        }


    }




    public function getExpressCheckout(Request $request)
    {
        $user = $this->userRepository->findByField('api_token', $request->get('api_token'))->first();
        $delivery_id = $request->get('delivery_address_id');
        if (!empty($user)) {
            $this->order->user = $user;
            $this->order->user_id = $user->id;
            $this->order->delivery_address_id = $delivery_id;
            $cart = $this->getCheckoutData($request->trip);
            try {
                return view('redeban.RedebanPayment', compact('cart', 'user', 'delivery_id'));
            } catch (\Exception $e) {
                session()->put(['code' => 'danger', 'message' => "Error processing payment for your order"]);
            }
        }
    }



    /**
     * Set cart data for processing payment on PayPal.
     *
     *
     * @return array
     */
    private function getCheckoutData($trip)
    {
        $data = [];
        $this->calculateTotal();
        $order_id = $this->paymentRepository->all()->count() + 1;
        $data['items'][] = [
            'name' => $this->order->user->cart[0]->product->market->name,
            'price' => $this->total + floatval($trip),
            'qty' => 1,
        ];
        $data['total'] = $this->total +  floatval($trip);
        $data['return_url'] = url("payments/redeban/express-checkout-success?user_id=" . $this->order->user_id. "&delivery_address_id=" . $this->order->delivery_address_id);
        $data['cancel_url'] = url('payments/redeban');
        $data['invoice_id'] = $order_id . '_' . date("Y_m_d_h_i_sa");
        $data['invoice_description'] = $this->order->user->cart[0]->product->market->name;
        return $data;
    }




    /**
     * Process payment on PayPal.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     * @throws ValidatorException
     * @throws \Exception
     */
    public function getExpressCheckoutSuccess(Request $request)
    {

        $this->order->user_id = $request->get('user_id', 0);
        $this->order->user = $this->userRepository->findWithoutFail($this->order->user_id);
        $this->order->delivery_address_id = $request->get('delivery_address_id', 0);

        //Log::info((string)$request->all());

        //$cart = $this->getCheckoutData();

        $this->order->payment = new Payment();
        $this->order->payment->status = 'succeeded';
        $this->order->payment->method = 'Redeban';
        //Log::info('success');

        $this->createOrder();

        //dd($this->order);


        return 'true';

    }
}
