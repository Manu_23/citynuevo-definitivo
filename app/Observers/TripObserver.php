<?php

namespace App\Observers;

use App\Models\Trip;

class TripObserver
{
    /**
     * Handle the trip "created" event.
     *
     * @param  \App\Models\Trip  $trip
     * @return void
     */
    public function created(Trip $trip)
    {

    }

    /**
     * Handle the trip "updated" event.
     *
     * @param  \App\Models\Trip  $trip
     * @return void
     */
    public function updated(Trip $trip)
    {
        //
    }

    /**
     * Handle the trip "deleted" event.
     *
     * @param  \App\Models\Trip  $trip
     * @return void
     */
    public function deleted(Trip $trip)
    {
        //
    }

    /**
     * Handle the trip "restored" event.
     *
     * @param  \App\Models\Trip  $trip
     * @return void
     */
    public function restored(Trip $trip)
    {
        //
    }

    /**
     * Handle the trip "force deleted" event.
     *
     * @param  \App\Models\Trip  $trip
     * @return void
     */
    public function forceDeleted(Trip $trip)
    {
        //
    }
}
