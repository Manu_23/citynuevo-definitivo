<?php

use Faker\Factory as Faker;
use App\Models\Vehicles;
use App\Repositories\VehiclesRepository;

trait MakeVehiclesTrait
{
    /**
     * Create fake instance of Vehicles and save it in database
     *
     * @param array $vehiclesFields
     * @return Vehicles
     */
    public function makeVehicles($vehiclesFields = [])
    {
        /** @var VehiclesRepository $vehiclesRepo */
        $vehiclesRepo = App::make(VehiclesRepository::class);
        $theme = $this->fakeVehiclesData($vehiclesFields);
        return $vehiclesRepo->create($theme);
    }

    /**
     * Get fake instance of Vehicles
     *
     * @param array $vehiclesFields
     * @return Vehicles
     */
    public function fakeVehicles($vehiclesFields = [])
    {
        return new Vehicles($this->fakeVehiclesData($vehiclesFields));
    }

    /**
     * Get fake data of Vehicles
     *
     * @param array $postFields
     * @return array
     */
    public function fakeVehiclesData($vehiclesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'role' => $fake->word,
            'des' => $fake->text,
            'imagen' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $vehiclesFields);
    }
}
