<?php namespace Tests\Repositories;

use App\Models\Vehicles;
use App\Repositories\VehiclesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class VehiclesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var VehiclesRepository
     */
    protected $vehiclesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->vehiclesRepo = \App::make(VehiclesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_vehicles()
    {
        $vehicles = factory(Vehicles::class)->make()->toArray();

        $createdVehicles = $this->vehiclesRepo->create($vehicles);

        $createdVehicles = $createdVehicles->toArray();
        $this->assertArrayHasKey('id', $createdVehicles);
        $this->assertNotNull($createdVehicles['id'], 'Created Vehicles must have id specified');
        $this->assertNotNull(Vehicles::find($createdVehicles['id']), 'Vehicles with given id must be in DB');
        $this->assertModelData($vehicles, $createdVehicles);
    }

    /**
     * @test read
     */
    public function test_read_vehicles()
    {
        $vehicles = factory(Vehicles::class)->create();

        $dbVehicles = $this->vehiclesRepo->find($vehicles->id);

        $dbVehicles = $dbVehicles->toArray();
        $this->assertModelData($vehicles->toArray(), $dbVehicles);
    }

    /**
     * @test update
     */
    public function test_update_vehicles()
    {
        $vehicles = factory(Vehicles::class)->create();
        $fakeVehicles = factory(Vehicles::class)->make()->toArray();

        $updatedVehicles = $this->vehiclesRepo->update($fakeVehicles, $vehicles->id);

        $this->assertModelData($fakeVehicles, $updatedVehicles->toArray());
        $dbVehicles = $this->vehiclesRepo->find($vehicles->id);
        $this->assertModelData($fakeVehicles, $dbVehicles->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_vehicles()
    {
        $vehicles = factory(Vehicles::class)->create();

        $resp = $this->vehiclesRepo->delete($vehicles->id);

        $this->assertTrue($resp);
        $this->assertNull(Vehicles::find($vehicles->id), 'Vehicles should not exist in DB');
    }
}
