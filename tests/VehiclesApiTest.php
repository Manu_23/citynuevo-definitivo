<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Vehicles;

class VehiclesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_vehicles()
    {
        $vehicles = factory(Vehicles::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/vehicles', $vehicles
        );

        $this->assertApiResponse($vehicles);
    }

    /**
     * @test
     */
    public function test_read_vehicles()
    {
        $vehicles = factory(Vehicles::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/vehicles/'.$vehicles->id
        );

        $this->assertApiResponse($vehicles->toArray());
    }

    /**
     * @test
     */
    public function test_update_vehicles()
    {
        $vehicles = factory(Vehicles::class)->create();
        $editedVehicles = factory(Vehicles::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/vehicles/'.$vehicles->id,
            $editedVehicles
        );

        $this->assertApiResponse($editedVehicles);
    }

    /**
     * @test
     */
    public function test_delete_vehicles()
    {
        $vehicles = factory(Vehicles::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/vehicles/'.$vehicles->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/vehicles/'.$vehicles->id
        );

        $this->response->assertStatus(404);
    }
}
